<?php

namespace frontend\controllers;

use common\library\API;
use Yii;
use common\models\Region;
use frontend\models\RegionSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RegionController implements the CRUD actions for Region model.
 */
class RegionController extends Controller
{
    //初始化province
    public function actionGetProvince()
    {
        $province = Region::find()->where(['level' => 0])->asArray()->all();


        return API::format(0,'',0,$province);
    }

    //根据province_id获取city
    public function actionGetCity()
    {
        $province_id = Yii::$app->request->get('province_id');

        $city = Region::find()->where(['parent_id' => $province_id])->asArray()->all();


        return API::format(0,'',0,$city);
    }

    //根据city_id获取district
    public function actionGetDistrict()
    {

        $city_id = Yii::$app->request->get('city_id');

        $district = Region::find()->where(['parent_id' => $city_id])->asArray()->all();


        return API::format(0,'',0,$district);
    }
}
