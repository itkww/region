<?php
/**
 * Created by PhpStorm.
 * User: Ideal
 * Date: 2018/4/18
 * Time: 16:26
 */
?>
    <form class="form-horizontal">
        <div class="form-group">
            <label for="province" class="col-sm-1 control-label">province</label>
            <div class="col-sm-3">
                <select class="form-control" name="province" id="province">
                    <option>==请选择===</option>
                </select>
            </div>


            <label for="city" class="col-sm-1 control-label">city</label>
            <div class="col-sm-3">
                <select class="form-control" name="city" id="city">
                    <option>==请选择===</option>
                </select>
            </div>

            <label for="district" class="col-sm-1 control-label">district</label>
            <div class="col-sm-3">
                <select class="form-control" name="district" id="district">
                    <option>==请选择===</option>
                </select>
            </div>
        </div>
    </form>
<?php

$script =<<<js
     
    $(function() {
        
        // 初始化省  
        initAddress();
        
        function initAddress() {
            $.ajax({
                type:'GET',
                url:'get-province',                
                success:function(data) {
                    var dataArr = [];
                    for (var i in data.data) {
                        dataArr.push(data.data[i]); //属性                        
                    }
                    $.each(dataArr, function(i, item) {
                        $("#province").append("<option value='"+item.id+"'>" + item.name + "</option>");
                    });
                }
            });
        }
        
        //change事件监听province改变，获取该省份下面的市
        $("#province").change(function() {
            var province_id = $("#province option:selected").val();
            getCity(province_id);
        });
        
        //change事件监听city改变，获取该省份下面的市
        $("#city").change(function() {
            var city_id = $("#city option:selected").val();
            getDistrict(city_id);
        });
        
        function getCity(province_id) {
            $("#city").find("option").remove(); 
            $.ajax({
                type:'GET',
                url:'get-city',
                data:{
                    province_id:province_id
                },
                success:function(data) {
                    var dataArr = [];
                    for (var i in data.data) {
                        dataArr.push(data.data[i]); //属性                        
                    }
                    $.each(dataArr, function(i, item) {
                        $("#city").append("<option value='"+item.id+"'>" + item.name + "</option>");
                    });
                }
            });
        }
        
        function getDistrict(city_id) {
            $("#district").find("option").remove(); 
            $.ajax({
                type:'GET',
                url:'get-district',
                data:{
                    city_id:city_id  
                },
                success:function(data) {
                    var dataArr = [];
                    for (var i in data.data) {
                        dataArr.push(data.data[i]); //属性                        
                    }
                    $.each(dataArr, function(i, item) {
                        $("#district").append("<option value='"+item.id+"'>" + item.name + "</option>");
                    });
                }
            });
        }
        
    });
js;
$this->registerJs($script);

